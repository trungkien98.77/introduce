import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: "I'm Kiên", // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: 'Welcome to my website, I wanted to show you who I was.', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: '',
  subtitle: '',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne:
    "Hi, I'm Lê Trung Kiên, a developer living in Ho Chi Minh city. I’m originally from Đăk Lăk province. I moved to HCM in 2016 as a student at UIT (University of Information Technology).",
  paragraphTwo:
    'So I find it very excited to discover new things. I’m a great fan of soccer and I always spend my time playing soccer or esports soccer game with my friends. I like country song and cooking. Specially, I love Son-kun, who is a cartoon character in Dragon Ball.',
  paragraphThree:
    'I’m eager to learn new things and willing to work in team. I easily adapt to new working environment and take initiative in work.',
  resume: '#', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    imgs: ['project1.png'],
    title:
      'Desktop Application - System of Management and Assessment for Student Program Outcome Standards',
    info:
      'Building applications capable of analyzing the quality of university training quantified based on subject Program Outcome Standards. In order to help students, faculty and administrators easily, monitor and manage information about training standards in a friendly and fast way.',
    info2: '',
    details: [
      'Created base on Java Swing in April 2019.',
      'Database: SQL Server.',
      'Team member: Trung Kien.',
    ],
    url: 'https://youtu.be/4xwlaBO9pm8',
    repo: 'https://gitlab.com/trungkien98.77/cong-nghe-java', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    imgs: ['project2.png'],
    title:
      'Web Application - Data sharing and management system base on Blockchain Hyperledger Composer',
    info:
      'The system helps members in the organization to manage personal data and share information through joint projects. User authentication uses JWT (JSON Web Token). User operation history is recorded by Blockchain Hyperledger Composer (private blockchain) such as create, move, copy, rename, share, delete,...',
    info2: '',
    details: [
      'Created in July 2019.',
      'Back-end: Java Spring Boot, Front-end: ReactJS, Blockchain network: Hyperledger Composer, Database: Postgres SQL.',
      'Team member: Trung Kien and 3 others. Roles: building blockchain network - Hyperledger Composer, maintaining back-end and front-end.',
    ],
    url: 'https://drive.google.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    imgs: ['project3-1.png', 'project3-2.png'],
    title: 'Course projects - Chatbot Viral using Manychat on Facebook Messenger',
    info:
      'Our Chatbot is created base on technology of ManyChat. It is a social media marketing tool that covers everything from helping you acquire new followers to sharing content automatically with them and even chatting with them when they have questions.',
    info2: '',
    details: [
      'Created in May 2019.',
      'Team member: Trung Kien and 4 others. Roles: building system flow and making report.',
    ],
    url: 'https://www.messenger.com/t/155681105138649',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    imgs: ['project4.png'],
    title: 'Company project - Resource Chains - Business Growth Services',
    info:
      'Using microservice architectures to build and deploy this systems base on Docker environment. Programming language: Springboot, SpringSecurity, PostgresSQL, ReactJS. Practice security configuration use JWT. Using AWS to send mail, save file, deploy projects. Important data is saved in Blockchain - Hyperledger Fabric.',
    info2: '',
    details: [
      'Execution time: 6 month.',
      'Team member: Trung Kien and 5 others. Roles: updating features (back-end) and maintaining blockchain network - Hyperledger Fabric.',
    ],
    url: 'https://resourcechains.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    imgs: ['project5-1.png', 'project5-2.png', 'project5-4.png'],
    title:
      'Graduation thesis - Building management system and tracing the origin of foods on Website using Blockchain',
    info:
      'Supply chain is the network of distributing the raw materials, processing it through intermediate and utilized by consumers. Blockchain technology is a distributed network of data. This technology provides tamper-proof data with strong authentication where the information about all the products are encrypted. The blockchain technology is united with the Internet of Things to provide the increased transparency and efficient supply chain.',
    info2: '',
    details: [
      'The components of the system: Blockchain network includes 6 nodes (private) and uses Hyperledger Fabric; Back-end consist of 3 sub-modules base on Java Spring Boot Framework and connects to the database - PostgresSQL; Front-end is maded from ReactJS yields two pages (Participants and Consumers); The system is deployed in Docker environment on Compute Engine (Google Cloud Platform).',
      'Execution time: 8 month (from Feb 2020 to Sep 2020)',
      'Team member: Trung Kien and @NgocDuy. Roles: Creating and updating Blockchain network, developing features (back-end) and creating a page (front-end) of the system.',
    ],
    url: 'https://drive.google.com/file/d/1An0A7cgMXoWatwuSsq1bRIMfOKPoa59Q/view?usp=sharing',
    repo: 'https://gitlab.com/kdtrace', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    imgs: ['project6.png'],
    title: 'Company project - Happy Hotel - hotel and related services.',
    info:
      'Happy Hotel is an online travel agency for lodging reservations, manage hotel for owner and supervisor.',
    info2: '',
    details: [
      'There are many technologies in the system. We are in charge of maintenance and adding new functionality in Web server with Java Servlet API (include Java, HTML, CSS, JavaScript, Database,...).',
      'Work is being done from March 2020 to now.',
      'Team member: Trung Kien and 4 others. Roles: Updating layout, reformatting logic, fixing security with cookie and session,...',
    ],
    url: 'https://happyhotel.jp/',
    repo: '', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: 'Email',
  email: 'trungkien98.77@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'facebook',
      url: 'https://www.facebook.com/trungkien98.77',
    },
    {
      id: nanoid(),
      name: 'phone',
      url: 'tel:+84363758968',
    },
    {
      id: nanoid(),
      name: 'skype',
      url: 'https://join.skype.com/invite/efoc5TQ0H1O8',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/trungkien9877/',
    },
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.com/trungkien98.77',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
